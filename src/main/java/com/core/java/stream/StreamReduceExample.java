package com.core.java.stream;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class StreamReduceExample {
    public static void main(String[] args) {
        List<Employee> list = new LinkedList<>();
        list.add(new Employee("1"));
        list.add(new Employee("2"));
        list.add(new Employee("3"));

        Integer sum = list
                .stream()
                .map(Employee::getSalary)
                .reduce(0, (Integer a, Integer b) -> Integer.sum(a, b));


        // More reduce example

        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);
        int result = numbers
                .stream()
                .reduce(0, (subtotal, element) -> subtotal + element);

//Or
        int result1 = numbers.stream().reduce(0, Integer::sum,Integer::sum);


        List<String> letters = Arrays.asList("a", "b", "c", "d", "e");
        String result2 = letters
                .stream()
                .reduce("", (partialString, element) -> partialString + element);


        String result3 = letters.stream().reduce("", String::concat);


        String result4 = letters
                .stream()
                .reduce(
                        "", (partialString, element) -> partialString.toUpperCase() + element.toUpperCase());

        //Parellel Stream
        // When a stream executes in parallel, the Java runtime splits the stream into multiple substreams.
        // In such cases, we need to use a function to combine the results of the substreams into a single one.
        // This is the role of the combiner – in the below snippet, it's the Integer::sum method reference.
        // Using combiner
        List<Integer> ages = Arrays.asList(25, 30, 45, 28, 32);
        int computedAges = ages.parallelStream().reduce(0, (a, b) -> a + b, Integer::sum);


        List<User> users = Arrays.asList(new User("John", 30), new User("Julie", 35));
        //int computedAges1 =  users.stream().reduce(0, (partialAgeResult, user) -> partialAgeResult + user.getAge());

        //  we have a stream of User objects, and the types of the accumulator arguments are Integer and User.
        //  However, the accumulator implementation is a sum of Integers, so the compiler just can't infer the type of the user parameter.

        // We can fix this issue by using a combiner:
        int result5 = users.stream()
                .reduce(0, (partialAgeResult, user) -> partialAgeResult + user.getAge(), Integer::sum);


    }
}

class Employee {
    private Integer salary;

    public Employee(String aSalary) {
        this.salary = Integer.valueOf(aSalary);
    }

    public Integer getSalary() {
        return this.salary;
    }
}


class User {
    private String name;
    private Integer age;

    public User(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }
}