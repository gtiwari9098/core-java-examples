package com.core.java.stream;

import java.util.stream.Stream;

public class TestExecution {
    public static void main(String[] args) {
        // Stream match() example
        Stream<Integer> numbers5 = Stream.of(1,2,3,4,5);
        Stream<Integer> numbers4 = Stream.of(1,2,3,4,5);
        Stream<Integer> numbers3 = Stream.of(1,2,3,4,5);
        System.out.println("Stream doesn't contain 10? "+numbers5.noneMatch(i -> i==10));
        System.out.println("Stream contains 4? "+numbers3.anyMatch(i -> i==4));
        System.out.println("Stream contains all elements less than 10? "+numbers4.allMatch(i -> i<10));



    }
}
