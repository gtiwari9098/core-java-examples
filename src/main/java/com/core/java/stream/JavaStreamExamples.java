package com.core.java.stream;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class JavaStreamExamples {

    public static void main(String [] args){
        Stream<Integer> stream = Stream.of(1,2,3,4); // Sequential ordered stream
        //Stream<Integer> stream = Stream.of(new Integer[]{1,2,3,4});
        List myList =stream.collect(Collectors.toList());
        Stream<Integer> sequentialStream = myList.stream(); // returns sequential stream
        Stream<Integer> parallelStream = myList.parallelStream(); // returns parallel stream
        Stream<String> stream1 = Stream.generate(() -> {return "abc";}); // returns infinite sequential unordered stream
        Stream<String> stream2 = Stream.iterate("abc", (i) -> i); // returns infinite sequential ordered stream
        System.out.println("-------");

       // stream1.forEach(System.out::println);
        System.out.println("-------");
      //  stream2.forEach(System.out::println);
        System.out.println("-------");

        LongStream is = Arrays.stream(new long[]{1,2,3,4});
        IntStream is2 = "abc".chars();

        Stream<Integer> intStream = Stream.of(1,2,3,4);
        List<Integer> intList = intStream.collect(Collectors.toList());
        Stream<Integer> intStreamSecond = Stream.of(1,2,3,4);
        Map<Integer,Integer> intMap = intStreamSecond.collect(Collectors.toMap(i -> i, i -> i+10));
        System.out.println(intMap); //prints {1=11, 2=12, 3=13, 4=14}


        // Stream Intermediate operation
        System.out.println("==============================   Stream Intermediate operation =======================================");
        Stream<Integer> highNums = sequentialStream.filter(p -> p > 90); //filter numbers greater than 90
        highNums.forEach(p -> System.out.print(p+" "));


        // Stream MAP example
        Stream<String> names = Stream.of("aBc", "d", "ef");
        System.out.println(names.map(s -> {
            return s.toUpperCase();
        }).collect(Collectors.toList()));
        //prints [ABC, D, EF]


        // Stream Sorted example

        Stream<String> names2 = Stream.of("aBc", "d", "ef", "123456");
        List<String> reverseSorted = names2.sorted(Comparator.reverseOrder()).collect(Collectors.toList()); // reverse order sorting
        System.out.println(reverseSorted); // [ef, d, aBc, 123456]

        Stream<String> names3 = Stream.of("aBc", "d", "ef", "123456");
        List<String> naturalSorted = names3.sorted().collect(Collectors.toList()); // natural ordered sorting
        System.out.println(naturalSorted); //[123456, aBc, d, ef]

        // Stream flatMap example
        Stream<List<String>> namesOriginalList = Stream.of(
                Arrays.asList("Pankaj"),
                Arrays.asList("David", "Lisa"),
                Arrays.asList("Amit"));
        //flat the stream from List<String> to String stream
        Stream<String> flatStream = namesOriginalList
                .flatMap(strList -> strList.stream());
        flatStream.forEach(System.out::println);

        // Java Stream terminal example
        System.out.println("======================================= TERMINAL OPERATION ========================================");
        Stream<Integer> numbers = Stream.of(1,2,3,4,5);
        Stream<Integer> numbers1 = Stream.of(1,2,3,4,5);

        // Stream reduce() example:
        Optional<Integer> intOptional = numbers.reduce((i,j) -> {return i*j;});
        if(intOptional.isPresent()) System.out.println("Multiplication = "+intOptional.get()); //120

        //Stream count() example
        System.out.println("Number of elements in stream="+numbers1.count()); //5


        //Stream forEach() example
        Stream<Integer> numbers2 = Stream.of(1,2,3,4,5);
        numbers2.forEach(i -> System.out.print(i+",")); //1,2,3,4,5,

        // Stream match() example
        Stream<Integer> numbers5 = Stream.of(1,2,3,4,5);
        Stream<Integer> numbers4 = Stream.of(1,2,3,4,5);
        Stream<Integer> numbers3 = Stream.of(1,2,3,4,5);
        System.out.println("Stream doesn't contain 10? "+numbers5.noneMatch(i -> i==10));
        System.out.println("Stream contains 4? "+numbers3.anyMatch(i -> i==4));
        System.out.println("Stream contains all elements less than 10? "+numbers4.allMatch(i -> i<10));

        //Stream findFirst() example

        Stream<String> names4 = Stream.of("Ram","Daya","Devi","krishna","David");
        Optional<String> firstNameWithD = names4.filter(i -> i.startsWith("D")).findFirst();
        if(firstNameWithD.isPresent()){
            System.out.println("First Name starting with D="+firstNameWithD.get()); //David
        }
    }
}
