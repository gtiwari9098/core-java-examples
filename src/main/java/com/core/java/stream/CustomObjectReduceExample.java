package com.core.java.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomObjectReduceExample {

    public static void main(String[] args) {
        UserOne john = new UserOne("John", 30);
        john.getRating().add(new Review(5, ""));
        john.getRating().add(new Review(3, "not bad"));
        UserOne julie = new UserOne("Julie", 35);
        john.getRating().add(new Review(4, "great!"));
        john.getRating().add(new Review(2, "terrible experience"));
        john.getRating().add(new Review(4, ""));
        List<UserOne> users = Arrays.asList(john, julie);

        //that John and Julie are accounted for, let's use Stream.reduce()
        // to compute an average rating across both users.
        // As an identity, let's return a new Rating if our input list is empty:

        Rating averageRating = users.stream()
                .reduce(new Rating(),
                        (rating, userOne) -> Rating.average(rating, userOne.getRating()),
                        Rating::average);

    }

}


class UserOne {
    private String name;
    private Integer age;
    private Rating rating;

    public UserOne(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public Rating getRating() {
        return rating;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }
}

class Rating {
    double points;
    List<Review> reviews = new ArrayList<>();

    public void add(Review review) {
        reviews.add(review);
        computeRating();
    }

    private double computeRating() {
        double totalPoints =
                reviews.stream().map(Review::getPoints).reduce(0, Integer::sum);
        this.points = totalPoints / reviews.size();
        return this.points;
    }

    public static Rating average(Rating r1, Rating r2) {
        Rating combined = new Rating();
        combined.reviews = new ArrayList<>(r1.reviews);
        combined.reviews.addAll(r2.reviews);
        combined.computeRating();
        return combined;
    }
}

class Review {

    private int points;
    private String review;

    public Review(int points, String review) {
        this.points = points;
        this.review = review;
    }

    public int getPoints() {
        return points;
    }

    public String getReview() {
        return review;
    }
}
