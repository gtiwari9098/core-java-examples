package com.core.java.stream;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class SortHashMapByValue {
    public static void main(String[] args) {

        HashMap<Integer, String> unsortedMap = new HashMap<Integer, String>();
        unsortedMap.put(1, "froyo");
        unsortedMap.put(2, "abby");
        unsortedMap.put(3, "denver");
        unsortedMap.put(4, "frost");
        unsortedMap.put(5, "daisy");

        Map<Integer, String> sortedMap =
                unsortedMap.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue())
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (e1, e2) -> e1, LinkedHashMap::new));
        System.out.println(sortedMap.toString());
    }
}
