package com.core.java.enumdir;

public class EnumExample {
    public static void main(String[] args) {
        Level level = Level.HIGH;
        System.out.println(level.getLevelCode());
    }

}

enum Level {
    HIGH(3),
    MEDIUM(2),
    LOW(2);

    private final int levelCode;

    Level(int levelCode) {
        this.levelCode = levelCode;
    }

    public int getLevelCode() {
        return levelCode;
    }
}
