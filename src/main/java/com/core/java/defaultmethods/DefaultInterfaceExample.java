package com.core.java.defaultmethods;

public class DefaultInterfaceExample implements First, Second {
    @Override
    public void add() {

    }

    @Override
    public Integer multiply(int x, int y) {
        return null;
    }

    @Override
    public void newAdd() {

    }
}

interface First {
    void add();

    default Integer multiply(int x, int y) {
        return x * y;
    }
}

interface Second {
    void newAdd();

    default Integer multiply(int x, int y) {
        return x * y;
    }
}
