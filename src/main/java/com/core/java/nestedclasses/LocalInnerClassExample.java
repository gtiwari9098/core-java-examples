package com.core.java.nestedclasses;

public class LocalInnerClassExample {
    private int data = 30;//instance variable

    void display() {
        class LocalClass {
            void msg() {
                System.out.println(data);
            }
        }
        LocalClass l = new LocalClass();
        l.msg();
    }

    public static void main(String args[]) {
        LocalInnerClassExample obj = new LocalInnerClassExample();
        obj.display();
    }
}
