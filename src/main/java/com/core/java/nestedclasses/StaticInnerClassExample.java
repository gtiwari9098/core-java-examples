package com.core.java.nestedclasses;

class StaticInnerClassExample{
    static private int data=30;
    static class Inner{
        void msg(){System.out.println("data is "+data);}
        static void operation(){System.out.println("operation  is "+data*2);}
    }
    public static void main(String args[]){
        StaticInnerClassExample.Inner obj=new StaticInnerClassExample.Inner();
        obj.msg();
        StaticInnerClassExample.Inner.operation();
    }
}