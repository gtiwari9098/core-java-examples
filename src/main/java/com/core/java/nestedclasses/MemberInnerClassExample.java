package com.core.java.nestedclasses;

public class MemberInnerClassExample {
    private int data = 30;

    class InnerCass {
        void msg() {
            System.out.println("data is " + data);
        }
    }

    public static void main(String args[]) {
        MemberInnerClassExample obj = new MemberInnerClassExample();
        MemberInnerClassExample.InnerCass in = obj.new InnerCass();
        in.msg();
    }
}
