package com.core.java.comparablecomparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ComparatorableExample {
    public static void main(String[] args) {
        List<Player> footballTeam = new ArrayList<>();
        Player player1 = new Player(59, "John", 20);
        Player player2 = new Player(67, "Roger", 22);
        Player player3 = new Player(45, "Steven", 24);
        footballTeam.add(player1);
        footballTeam.add(player2);
        footballTeam.add(player3);

        System.out.println("Before Sorting : " + footballTeam);
        Collections.sort(footballTeam);
        System.out.println("After Sorting : " + footballTeam);

        // Java 8 comparator
        Comparator<Player> byRanking
                = (Player playerF,  Player playerS) -> playerF.getRanking() - playerS.getRanking();

        Collections.sort(footballTeam,byRanking);
        System.out.println("After Sorting  by ranking: " + footballTeam);


        Comparator<Player> byRankingAnother = Comparator.comparing(Player::getRanking);

        Comparator<Player> byAge = Comparator.comparing(Player::getAge);

        Comparator<Player> byAgenStaticMethod = Player::compareAge;

        List<Player> reverseSorted = footballTeam.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList()); // reverse order sorting
        System.out.println(reverseSorted);


        // two level sorting
        footballTeam.sort(Comparator.comparing(Player::getAge).thenComparing(Player::getName));
    }
}



class Player  implements  Comparable<Player> {
    private int ranking;
    private String name;
    private int age;

    public Player() {
    }


    public Player(int ranking, String name, int age) {
        this.ranking = ranking;
        this.name = name;
        this.age = age;
    }


    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    static int compareAge(Player P1, Player P2){
        return P1.compareTo(P2);
    }
    @Override
    public int compareTo(Player otherPlayer) {
        return (this.getRanking() - otherPlayer.getRanking());
    }

    @Override
    public String toString() {
        return "Player{" +
                "ranking=" + ranking +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}



class PlayerRankingComparator implements Comparator<Player> {

    @Override
    public int compare(Player firstPlayer, Player secondPlayer) {
        return (firstPlayer.getRanking() - secondPlayer.getRanking());
    }
}

class PlayerAgeComparator implements Comparator<Player> {
    @Override
    public int compare(Player firstPlayer, Player secondPlayer) {
        return (firstPlayer.getAge() - secondPlayer.getAge());
    }
}