package com.core.java.jdbc;

import java.sql.*;

//pstmnt.execute			if the query return only one result					 boolean execute() throws SQLException;
//pstmnt.executeUpdate			if the query doesnot return a resultset					int executeUpdate() throws SQLException;
//pstmnt.executeQuery			if the query might return more than one resultset					ResultSet executeQuery() throws SQLException;
// default long executeLargeUpdate() throws SQLException
public class JdbcDemo {
    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection("url", "username", "password");
            connection.setAutoCommit(false);
            Statement stmt = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, ResultSet.HOLD_CURSORS_OVER_COMMIT);
            // ResultSet.TYPE_FORWARD_ONLY
            // ResultSet.TYPE_SCROLL_INSENSITIVE
            // ResultSet.TYPE_SCROLL_SENSITIVE
            // ResultSet.CONCUR_READ_ONLY
            // ResultSet.CONCUR_UPDATABLE
            stmt.addBatch("");
            stmt.executeBatch();

            Savepoint save1 = connection.setSavepoint();
            connection.rollback(save1);
            connection.releaseSavepoint(save1);

            ResultSet result = stmt.executeQuery("select * from people");

            //Once a PreparedStatement is prepared, it can be reused after execution.
            // You reuse a PreparedStatement by setting new values for the parameters and then execute it again

           // String    sqlOne      = "update people set name='John' where id=123";
            String sqlu = "update people set firstname=? , lastname=? where id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlu);
            preparedStatement.setString(1, "Gary");
            preparedStatement.setString(2, "Larson");
            preparedStatement.setLong(3, 123);
            int rowsAffected = preparedStatement.executeUpdate(sqlu);

            String sqlPrepared = "select * from people where firstname=? and lastname=?";
            PreparedStatement preparedStatement1 = connection.prepareStatement(sqlPrepared);

            preparedStatement1.setString(1, "John");
            preparedStatement1.setString(2, "Smith");
            ResultSet result1 = preparedStatement1.executeQuery();

            while (result1.next()) {
                String employeeName = result1.getString("EMP_NAME");
                int employeeId = result1.getInt("EMP_ID");
                float salary = result1.getFloat("SALARY");
                int agr = result1.getInt("AGE");
            }

            CallableStatement callableStatement = connection.prepareCall("sql");
            ResultSet resultCall = callableStatement.executeQuery();
            //The executeQuery() method is used if the stored procedure returns a ResultSet.

            CallableStatement callableStatement1 =
                    connection.prepareCall("{call calculateStatistics(?, ?)}");

            callableStatement1.setString(1, "param1");
            callableStatement1.setInt(2, 123);

            callableStatement1.executeUpdate();
// if a stored procedure contains an OUT parameter, you must register it with the registerOutParameter method.
            callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
            callableStatement.registerOutParameter(2, java.sql.Types.INTEGER);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

}
