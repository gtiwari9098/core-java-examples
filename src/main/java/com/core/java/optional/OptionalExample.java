package com.core.java.optional;


import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public class OptionalExample {
    public static void main (String [] args) {


        Optional<String> empty = Optional.empty();
        //Returns the hash code value of the present value, if any, or 0 (zero) if no value is present.
        //If a value is present, it invokes the specified consumer with the value, otherwise does nothing.
        Optional<String> optIFPresent = Optional.of("baeldung"); // throws null pointer exception is value is null
        optIFPresent.ifPresent(name -> System.out.println(name.length()));

        //Returns an Optional with the specified present non-null value.
        String nameString = "baeldung";
        Optional<String> opt = Optional.of(nameString);
        System.out.println(opt.isPresent());

        String nullNameO = null;
        //Optional<String> optOfNllable = Optional.of(nullNameO); // will throw null pointer exception
        //System.out.println(optOfNllable.isPresent()); // false

        //Returns an Optional describing the specified value, if non-null,
        // otherwise returns an empty Optional.
        String name = "baeldung";
        Optional<String> optionalString = Optional.ofNullable(name);
        System.out.println(optionalString.get());


        String nullableName = null;
        Optional<String> nullableOptional = Optional.ofNullable(nullableName);
        ////will not throw nullPointerException// rather return a empty optional object
        try {
            String blank = nullableOptional.get();
            System.out.println(); // throws NuSuchelementException
        } catch(NoSuchElementException nse){
            nse.printStackTrace();
        }


        //Returns the value if present, otherwise returns other.
        String nullName = null;
        String nameResult = Optional.ofNullable(nullName).orElse("john");
        System.out.println("OrElse => "+nameResult);

        // Returns the value if present, otherwise invokes other and returns the result of that invocation.
        String nullNameOrElseget = null;
        String nameNewResult = Optional.ofNullable(nullNameOrElseget).orElseGet(() -> "john");
        System.out.println("OrElseGet => "+nameNewResult);

        //Returns the contained value, if present,
        // otherwise throws an exception to be created by the provided supplier.
        String nullNameElseThrow = null;
        String nameOrElseThrow = Optional.ofNullable(nullName).orElseThrow(
                IllegalArgumentException::new);

        // If a value is present, applies the provided mapping function to it, and if the result is non-null, returns an Optional describing the result.
        List<String> companyNames = Arrays.asList("paypal", "oracle", "", "microsoft", "", "apple");
        Optional<List<String>> listOptional = Optional.of(companyNames);
        int size = listOptional.map(List::size).orElse(0);

        //filter method simply performs a check on the value and returns a boolean
        //the map method takes the existing value, performs a computation using this value and returns the result of the computation wrapped in an Optional object:
        String password = " password ";
        Optional<String> passOpt = Optional.of(password);
        boolean correctPassword = passOpt.filter(pass -> pass.equals("password")).isPresent();
        correctPassword = passOpt
                .map(String::trim)
                .filter(pass -> pass.equals("password"))
                .isPresent();
    }
}
