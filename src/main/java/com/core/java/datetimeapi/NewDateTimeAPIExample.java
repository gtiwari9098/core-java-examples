package com.core.java.datetimeapi;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Locale;

public class NewDateTimeAPIExample {
    public static void main (String[] args) {
        //represents a date in ISO format ( yyyy-MM-dd) without time
        LocalDate localDate = LocalDate.now();
        LocalDate.of(2015, 02, 20);
        LocalDate.parse("2015-02-20");
        LocalDate tomorrow = LocalDate.now().plusDays(1);
        LocalDate previousMonthSameDay = LocalDate.now().minus(1, ChronoUnit.MONTHS);
        DayOfWeek sunday = LocalDate.parse("2016-06-12").getDayOfWeek();
        int twelve = LocalDate.parse("2016-06-12").getDayOfMonth();
        boolean leapYear = LocalDate.now().isLeapYear();
        boolean notBefore = LocalDate.parse("2016-06-12").isBefore(LocalDate.parse("2016-06-11"));
        boolean isAfter = LocalDate.parse("2016-06-12").isAfter(LocalDate.parse("2016-06-11"));
        LocalDateTime beginningOfDay = LocalDate.parse("2016-06-12").atStartOfDay();
        LocalDate firstDayOfMonth = LocalDate.parse("2016-06-12").with(TemporalAdjusters.firstDayOfMonth());

       // represents time without date
        LocalTime now = LocalTime.now();
        LocalTime sixThirtyParse = LocalTime.parse("06:30");
        LocalTime sixThirty = LocalTime.of(6, 30);
        LocalTime sevenThirty = LocalTime.parse("06:30").plus(1, ChronoUnit.HOURS);
        int six = LocalTime.parse("06:30").getHour();
        boolean isbefore = LocalTime.parse("06:30").isBefore(LocalTime.parse("07:30"));
        LocalTime maxTime = LocalTime.MAX;

        //
        // represent combination of  date and time
        LocalDateTime.now();
        LocalDateTime.of(2015, Month.FEBRUARY, 20, 06, 30);
        LocalDateTime localDateTime= LocalDateTime.parse("2015-02-20T06:30:00");
        localDateTime.plusDays(1);
        localDateTime.minusHours(2);
        localDateTime.getMonth();

        //Zoned DateTime
        LocalDateTime localDateTimeNew = LocalDateTime.of(2015, Month.FEBRUARY, 20, 06, 30);
        ZoneOffset offset = ZoneOffset.of("+02:00");
        OffsetDateTime offSetByTwo = OffsetDateTime
                .of(localDateTimeNew, offset);

        //DateTime Formatter
        LocalDateTime localDateTimeSecond = LocalDateTime.of(2015, Month.JANUARY, 25, 6, 30);

        String localDateString = localDateTimeSecond.format(DateTimeFormatter.ISO_DATE);

        localDateTime.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));


        localDateTimeSecond.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).withLocale(Locale.UK));
    }
}
