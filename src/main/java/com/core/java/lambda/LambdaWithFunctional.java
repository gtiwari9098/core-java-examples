package com.core.java.lambda;

import java.util.Arrays;

// operation is implemented using lambda expressions
interface FuncInter1 {
    int operation(int a, int b);
}

// sayMessage() is implemented using lambda expressions above
interface FuncInter2 {
    void sayMessage(String message);
}

public class LambdaWithFunctional {
    // Performs FuncInter1's operation on 'a' and 'b'
    private int operate(int a, int b, FuncInter1 fobj) {
        return fobj.operation(a, b);
    }

    public static void main(String args[]) {
        // lambda expression for addition for two parameters
        // data type for x and y is optional.
        // This expression implements 'FuncInter1' interface
        FuncInter1 add = (int x, int y) -> x + y;

        // lambda expression multiplication for two parameters
        // This expression also implements 'FuncInter1' interface
        FuncInter1 multiply = (x,y) -> x * y;

        // Creating an object of Test to call operate using
        // different implementations using lambda Expressions
        LambdaWithFunctional tobj = new LambdaWithFunctional();

        // Add two numbers using lambda expression
        System.out.println("Addition is " +
                tobj.operate(6, 3, add));

        // Multiply two numbers using lambda expression
        System.out.println("Multiplication is " +
                tobj.operate(6, 3, multiply));

        // lambda expression for single parameter
        // This expression implements 'FuncInter2' interface
        FuncInter2 fobj = message -> System.out.println("Hello "
                + message);
        fobj.sayMessage("Geek");

        // Runnable with lambda expression
        new Thread(
                () ->   {
                    System.out.println("My Runnable");
                }
        ).start();


        // Sorting employees by their name
        Employee[] employees  = {
                new Employee("David"),
                new Employee("Naveen"),
                new Employee("Alex"),
                new Employee("Richard")};

        System.out.println("Before Sorting Names: "+ Arrays.toString(employees));
        Arrays.sort(employees, Employee::nameCompare);
        System.out.println("After Sorting Names "+Arrays.toString(employees));
    }
}

class Employee {
    String name;

    Employee(String name) {
        this.name = name;
    }

    public static int nameCompare(Employee a1, Employee a2) {
        return a1.name.compareTo(a2.name);
    }

    public String toString() {
        return name;
    }
}