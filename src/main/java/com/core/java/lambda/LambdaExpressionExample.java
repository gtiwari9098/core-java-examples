package com.core.java.lambda;

import java.util.ArrayList;

// Lambda expression (or function) is just an anonymous function,
// i.e., a function with no name and without being bounded to an identifier.
public class LambdaExpressionExample {
    public static void main(String args[])
    {
        // lambda expression to implement above
        // functional interface. This interface
        // by default implements abstractFun()
        FuncInterface fobj = (int x)->System.out.println(2*x);
        // This calls above lambda expression and prints 10.
        fobj.abstractFun(5);


        // Creating an ArrayList with elements
        // {1, 2, 3, 4}
        ArrayList<Integer> arrL = new ArrayList<Integer>();
        arrL.add(1);
        arrL.add(2);
        arrL.add(3);
        arrL.add(4);

        // Using lambda expression to print all elements
        // of arrL
        arrL.forEach(n -> System.out.println(n));

        arrL.forEach(System.out::println); // same as above

        // Using lambda expression to print even elements
        // of arrL
        arrL.forEach(n -> { if (n%2 == 0) System.out.println(n); });
    }
}

// Java program to demonstrate lambda expressions
// to implement a user defined functional interface.

// A sample functional interface (An interface with
// single abstract method
interface FuncInterface
{
    // An abstract function
    void abstractFun(int x);


    // A non-abstract (or default) function
    default void normalFun()
    {
        System.out.println("Hello");
    }
}