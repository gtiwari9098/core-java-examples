package com.core.java.functionalinterface;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

public class BiFunctionExample {
    public static void main(String[] args) {
        simpleBiFunctionExample();
        bifunctionNumberExample();
        anotherexample();

    }

    private static void simpleBiFunctionExample() {
        List<String> stringList = Arrays.asList("a", "b", "c");
        List<Integer> integerList = Arrays.asList(1, 2, 3);
        List<String> resultList = listCombiner(stringList, integerList, (a, b) -> a + b);
        resultList.forEach(System.out::println);
    }

    private static <T, U, R> List<R> listCombiner(List<T> list1, List<U> list2, BiFunction<T, U, R> combinerFunc) {
        List<R> result = new ArrayList<>();

        for (int i = 0; i < list1.size(); i++) {
            result.add(combinerFunc.apply(list1.get(i), list2.get(i)));
        }
        return result;
    }

    private static void anotherexample() {

        Double d = ((BiFunction<String, String, Integer>) String::indexOf)
                .andThen(Integer::doubleValue)
                .apply("banana", "nan");
        System.out.println(d);
    }

    private static void bifunctionNumberExample() {
        BiFunction<Double, String, Long> adderToLong = (d, s) ->
                (long) (d + Long.parseLong(s));

        Function<Long, BigDecimal> bigDecimalConverter = l -> BigDecimal.valueOf(l);

        BiFunction<Double, String, BigDecimal> biFunction = adderToLong
                .andThen(bigDecimalConverter);

        BigDecimal bd = biFunction.apply(20.33d, "34");
        System.out.println(bd);
    }
}
