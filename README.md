Core Java concepts example with java 8

* Interface (  Default method/ static method
* Abstract Class
* overloading
* overriding
* Serialization
* Cloning
* Enum
* JDBC
* List classes
* Set clases
* Map classes
* Lambda Expressions
* Streaming
* Functional Interface
* garbage collection
* Local inner class
* inner class
* static inner class
* anonymous inner class

